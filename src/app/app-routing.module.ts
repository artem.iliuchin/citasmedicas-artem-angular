import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { CitaListComponent } from './citas/cita-list/cita-list.component';
import { DiagnosticoListComponent } from './diagnosticos/diagnostico-list/diagnostico-list.component';
import { MedicoListComponent } from './medicos/medico-list/medico-list.component';
import { PacienteListComponent } from './pacientes/paciente-list/paciente-list.component';
import { CitaEditarComponent } from './citas/cita-editar/cita-editar.component';
import { DiagnosticoEditarComponent } from './diagnosticos/diagnostico-editar/diagnostico-editar.component';
import { MedicoEditarComponent } from './medicos/medico-editar/medico-editar.component';
import { PacienteEditarComponent } from './pacientes/paciente-editar/paciente-editar.component';
import { RegistrarComponent } from './registrar/registrar.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'registrar', component: RegistrarComponent },
  //citas
  { path: 'citas', component: CitaListComponent, canActivate: [AuthGuard] },
  { path: 'citas/editar', component: CitaEditarComponent, canActivate: [AuthGuard] },
  //diagnosticos
  { path: 'diagnosticos', component: DiagnosticoListComponent, canActivate: [AuthGuard] },
  { path: 'diagnosticos/editar', component: DiagnosticoEditarComponent, canActivate: [AuthGuard] },
  //medicos
  { path: 'medicos', component: MedicoListComponent, canActivate: [AuthGuard] },
  { path: 'medicos/editar', component: MedicoEditarComponent, canActivate: [AuthGuard] },
  { path: 'medicos/:username', component: HomeComponent, canActivate: [AuthGuard] },
  //pacientes
  { path: 'pacientes', component: PacienteListComponent, canActivate: [AuthGuard] },
  { path: 'pacientes/editar', component: PacienteEditarComponent, canActivate: [AuthGuard] },
  { path: 'pacientes/:username', component: HomeComponent , canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

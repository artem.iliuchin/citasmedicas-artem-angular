import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { MedicoService } from '../services/medico.service';
import { PacienteService } from '../services/paciente.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username!: string;
  password!: string;

  constructor(private authService: AuthService, private router: Router,private pacienteService:PacienteService,private medicoService: MedicoService) {}

  onSubmit() {
    this.authService.login(this.username, this.password)
      .subscribe(
        () => {
          this.guardarId(this.username)
          this.router.navigate(['/']);

        },
        error => {
          console.log(error);
        }
      )
  }

  //Nesesito cambiar , a backend para que servicor indica y devuelve todo que necesito 
  guardarId(username:string){

    this.medicoService.getByUsername(username).subscribe((medico) => {
      if (medico != null) {
        this.medicoService.setMedicoId(medico.id)
      }
    })

    this.pacienteService.getByUsername(username).subscribe((paciente) => {
      if (paciente != null) {
        this.pacienteService.setPacienteId(paciente.id)
      }
    })

  }

}

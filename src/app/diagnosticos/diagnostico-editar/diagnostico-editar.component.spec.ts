import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnosticoEditarComponent } from './diagnostico-editar.component';

describe('DiagnosticoEditarComponent', () => {
  let component: DiagnosticoEditarComponent;
  let fixture: ComponentFixture<DiagnosticoEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiagnosticoEditarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DiagnosticoEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

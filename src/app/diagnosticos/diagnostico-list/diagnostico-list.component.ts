import { Component, OnInit } from '@angular/core';
import { DiagnosticoDTO } from 'src/app/modelos/diagnostico-dto.model';
import { DiagnosticoService } from 'src/app/services/diagnostico.service';

@Component({
  selector: 'app-diagnostico-list',
  templateUrl: './diagnostico-list.component.html',
  styleUrls: ['./diagnostico-list.component.css']
})
export class DiagnosticoListComponent implements OnInit {
  diagnosticos:DiagnosticoDTO[] = []

  constructor(private diagnosticoService:DiagnosticoService){}

  ngOnInit(): void{
    this.fetchDiagnosticos()
  }

  fetchDiagnosticos(): void{
    this.diagnosticoService.getAll().subscribe((diagnosticos) => {
      this.diagnosticos = diagnosticos
    })
  }

  editarDiagnostico(id: number): void{
    //Implementar logico para editar un diagnostico
  }

  deleteDiagnostico(id: number): void{
    this.diagnosticoService.delete(id).subscribe(() => {
      this.fetchDiagnosticos()//actulizar la lista despues de eliminar un diagnostico
    })
  }

}

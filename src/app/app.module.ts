import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthGuard } from './auth.guard';
import { TokenInterceptor } from './token.interceptor';
import { AuthService } from './auth.service';
import { CitaListComponent } from './citas/cita-list/cita-list.component';
import { AppRoutingModule } from './app-routing.module';
import { DiagnosticoListComponent } from './diagnosticos/diagnostico-list/diagnostico-list.component';
import { MedicoListComponent } from './medicos/medico-list/medico-list.component';
import { PacienteListComponent } from './pacientes/paciente-list/paciente-list.component';
import { PacienteEditarComponent } from './pacientes/paciente-editar/paciente-editar.component';
import { MedicoEditarComponent } from './medicos/medico-editar/medico-editar.component';
import { CitaEditarComponent } from './citas/cita-editar/cita-editar.component';
import { DiagnosticoEditarComponent } from './diagnosticos/diagnostico-editar/diagnostico-editar.component';
import { RegistrarComponent } from './registrar/registrar.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    CitaListComponent,
    DiagnosticoListComponent,
    MedicoListComponent,
    PacienteListComponent,
    PacienteEditarComponent,
    MedicoEditarComponent,
    CitaEditarComponent,
    DiagnosticoEditarComponent,
    RegistrarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    })
  ],
  providers: [
    AuthGuard,
    AuthService,
    JwtHelperService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { MedicoDTO } from '../modelos/medico-dto-model';
import { PacienteDTO } from '../modelos/paciente-dto.model';
import { MedicoService } from '../services/medico.service';
import { PacienteService } from '../services/paciente.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent {
  usuario: MedicoDTO | PacienteDTO = new PacienteDTO();
  tipoUsuario: 'medico' | 'paciente' = 'paciente';

  constructor(private authService: AuthService,private pacienteService: PacienteService,private medicoService:MedicoService,private router: Router){}

  onTipoUsuarioChange(value: 'medico' | 'paciente') {
    this.tipoUsuario = value
    if (value === 'medico') {
      this.usuario = new MedicoDTO()
    } else {
      this.usuario = new PacienteDTO()
    }
  }

  asPaciente(usuario: MedicoDTO | PacienteDTO): PacienteDTO {
    return usuario as PacienteDTO
  }

  asMedico(usuario: MedicoDTO | PacienteDTO): MedicoDTO {
    return usuario as MedicoDTO
  }

  onSubmit() {
    if(this.tipoUsuario === 'medico'){
      this.medicoService.create(this.asMedico(this.usuario)).subscribe(() => {
        this.router.navigate(['/']);
      })
    }else{
      this.pacienteService.create(this.asPaciente(this.usuario)).subscribe(() => {
        this.router.navigate(['/']);
      })
    }


  }
}

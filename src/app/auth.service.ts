import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {}

  login(username: string, password: string) {
    return this.http.post<any>(`login`, { username, password })
      .pipe(map(response => {
        if (response && response.token) {
          localStorage.setItem('token', response.token)
        }
        return response;
      }));
  }

  logout() {
    localStorage.removeItem('token')
  }

  getToken() {
    return localStorage.getItem('token')
  }

  isLoggedIn() {
    const token = this.getToken();
    return token ? !this.jwtHelper.isTokenExpired(token) : false
  }
}

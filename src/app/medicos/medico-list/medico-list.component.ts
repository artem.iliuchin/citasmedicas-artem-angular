import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MedicoDTO } from 'src/app/modelos/medico-dto-model';
import { MedicoService } from 'src/app/services/medico.service';

@Component({
  selector: 'app-medico-list',
  templateUrl: './medico-list.component.html',
  styleUrls: ['./medico-list.component.css']
})
export class MedicoListComponent implements OnInit{
  medicos: MedicoDTO[]=[];

  constructor(private medicoService: MedicoService, private router:Router){}

  ngOnInit(): void {
    this.fetchMedicos()
  }

  fetchMedicos(): void{
    this.medicoService.getAll().subscribe((medicos)=>{
      this.medicos = medicos;
    })
  }

  editMedico(id: number): void{
    this.medicoService.setMedicoId(id)
    this.router.navigate(["medicos/editar"]);
  }

  tomarCita(id: number): void{
    this.medicoService.setMedicoId(id)
    this.router.navigate(["citas/editar"]);
  }

  deleteMedico(id: number): void{
    this.medicoService.delete(id).subscribe(()=>{
      this.fetchMedicos();//actulizar la lista despues de eliminar un medico
    })
  }


}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PacienteDTO } from 'src/app/modelos/paciente-dto.model';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-paciente-list',
  templateUrl: './paciente-list.component.html',
  styleUrls: ['./paciente-list.component.css']
})
export class PacienteListComponent implements OnInit {
  pacientes: PacienteDTO[] = [];

  constructor(private pacienteService: PacienteService,private router:Router){}

  ngOnInit(): void {
    this.fetchPacientes()
  }

  fetchPacientes(): void{
    this.pacienteService.getAll().subscribe((pacientes)=>{
      this.pacientes = pacientes
    })
  }

  editPaciente(id: number): void{
    this.pacienteService.setPacienteId(id)
    this.router.navigate(["pacientes/editar"]);

  }

  deletePaciente(id: number): void{
    this.pacienteService.delete(id).subscribe(()=>{
      this.fetchPacientes()//actulizar la lista despues de eliminar un paciente
    })
  }


}

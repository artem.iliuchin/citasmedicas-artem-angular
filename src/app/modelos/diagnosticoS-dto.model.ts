export interface DiagnosticoSDTO {
  id:number;
  valoracionEspecialista:String;
  enfermedad:String;
}

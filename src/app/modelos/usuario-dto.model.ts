export class UsuarioDTO {
  id!: number;
  usuario!: string;
  nombre!: string;
  apellidos!: string;
  clave!: string;
}

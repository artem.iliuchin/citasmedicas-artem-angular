import { CitaDTO } from "./cita-dto.model"
import { PacienteSDTO } from "./pacienteS-dto"
import { UsuarioDTO } from "./usuario-dto.model"

export class MedicoDTO extends UsuarioDTO{
  numColegiado!:String
  pacientes!:PacienteSDTO[]
  citas!:CitaDTO[]

  constructor() {
    super();
  }


}

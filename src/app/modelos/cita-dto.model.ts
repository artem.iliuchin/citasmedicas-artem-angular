import { DiagnosticoSDTO } from "./diagnosticoS-dto.model";
import { MedicoSDTO } from "./medicoS-dto.model";
import { PacienteSDTO } from "./pacienteS-dto";

export interface CitaDTO {
  id: number;
  fechaHora: Date;
  motivoCita: string;
  medico: MedicoSDTO;
  paciente: PacienteSDTO;
  diagnostico: DiagnosticoSDTO[];
}

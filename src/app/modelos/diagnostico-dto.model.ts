import { CitaDTO } from "./cita-dto.model";

export interface DiagnosticoDTO{
  id:number;
  valoracionEspecialista:String;
  enfermedad:String;
  cita:CitaDTO;
}

import { CitaDTO } from "./cita-dto.model"
import { MedicoSDTO } from "./medicoS-dto.model"
import { UsuarioDTO } from "./usuario-dto.model"

export class PacienteDTO extends UsuarioDTO{
  nss!:String
  numTarjeta!:String
  telefono!:String
  direccion!:String
  medicos!:MedicoSDTO[]
  citas!:CitaDTO[]


  constructor() {
    super();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CitaDTO } from 'src/app/modelos/cita-dto.model';
import { CitaService } from 'src/app/services/cita.service';

@Component({
  selector: 'app-cita-list',
  templateUrl: './cita-list.component.html',
  styleUrls: ['./cita-list.component.css']
})
export class CitaListComponent implements OnInit {
  citas: CitaDTO[] = []

  constructor(private citaService: CitaService, private router:Router) {}

  ngOnInit(): void {
    this.fetchCitas()
  }

  fetchCitas(): void {
    this.citaService.getAll().subscribe((citas) => {
      this.citas = citas
    })
  }

  editCita(id: number): void {
    this.citaService.setCitaId(id)
    this.router.navigate(["citas/editar"])
  }

  deleteCita(id: number): void {
    this.citaService.delete(id).subscribe(() => {
      this.fetchCitas() // Actualiza la lista después de eliminar una cita
    })
  }

  addDiagnosticos(id: number):void{
    this.citaService.setCitaId(id)
    this.router.navigate(["diagnosticos/editar"])
  }

}

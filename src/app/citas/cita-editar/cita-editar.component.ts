import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { CitaDTO } from 'src/app/modelos/cita-dto.model';
import { DiagnosticoSDTO } from 'src/app/modelos/diagnosticoS-dto.model';
import { MedicoSDTO } from 'src/app/modelos/medicoS-dto.model';
import { PacienteSDTO } from 'src/app/modelos/pacienteS-dto';
import { CitaService } from 'src/app/services/cita.service';
import { MedicoService } from 'src/app/services/medico.service';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-cita-editar',
  templateUrl: './cita-editar.component.html',
  styleUrls: ['./cita-editar.component.css']
})
export class CitaEditarComponent implements OnInit {
  citaId: number
  medicoId: number
  pacienteId: number
  cita: CitaDTO
  private subscription!: Subscription

  constructor(private citaService: CitaService,private pacienteService:PacienteService,private medicoService: MedicoService,private route:Router) {
    this.cita = {
      id: 0,
      fechaHora: new Date(),
      motivoCita: '',
      medico: {} as MedicoSDTO,
      paciente: {} as PacienteSDTO,
      diagnostico: [] as DiagnosticoSDTO[],
    }
    this.citaId=0
    this.medicoId=0
    this.pacienteId=0
  }

  ngOnInit(): void {
    let encontrado=true
    this.subscription = this.citaService.citaId$.subscribe((id) => {
      if (id !== null) {
        this.citaId = id
        this.getCita()
      }else{
        encontrado=false
      }
    })
    if(!encontrado){
      this.medicoPaciente()
    }
    this.cita.fechaHora = new Date(this.formatDateToDatetimeLocal(this.cita.fechaHora))
  }


  medicoPaciente(){
    this.subscription = this.medicoService.medicoId$.subscribe((id) => {
      if (id !== null) {
        this.medicoId = id
      }
    })

    this.subscription = this.pacienteService.pacieteId$.subscribe((id) => {
      if (id !== null) {
        this.pacienteId = id
      }
    })

    this.pacienteService.getById(this.pacienteId).subscribe((paciente)=>{
      this.cita.paciente=paciente
    })

    this.medicoService.getById(this.medicoId).subscribe((medico)=>{
      this.cita.medico=medico
    })
  }

  formatDateToDatetimeLocal(date: Date): string {
    return formatDate(date, 'yyyy-MM-dd', 'en-US')
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }

  getCita(): void {
    this.citaService.getById(this.citaId).subscribe((cita) => {
      this.cita = cita;
    })
  }

  onSubmit(): void {
    if(this.pacienteId == 0 && this.medicoId== 0){
      this.citaService.update(this.cita).subscribe(() => {
        this.route.navigate([`/citas`])
      })
    }else{
      this.citaService.create(this.cita).subscribe(() => {
        this.route.navigate([`/citas`])
      })
    }
  }
}

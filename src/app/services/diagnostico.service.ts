import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DiagnosticoDTO } from '../modelos/diagnostico-dto.model';


@Injectable({
  providedIn: 'root'
})
export class DiagnosticoService {

  private api = "diagnosticos";

  private diagnosticoIdSource = new BehaviorSubject<number | null>(null)
  diagnosticoId$ = this.diagnosticoIdSource.asObservable()

  constructor(private http: HttpClient) { }

  setDiagnosticoId(id: number | null): void{
    this.diagnosticoIdSource.next(id)
  }

  getAll(): Observable<DiagnosticoDTO[]> {
    return this.http.get<DiagnosticoDTO[]>(this.api)
  }

  getById(id: number): Observable<DiagnosticoDTO> {
    return this.http.get<DiagnosticoDTO>(`${this.api}/${id}`)
  }

  update(diagnosticoDTO: DiagnosticoDTO): Observable<DiagnosticoDTO>{
    return this.http.put<DiagnosticoDTO>(this.api,diagnosticoDTO)
  }

  create(diagnosticoDTO: DiagnosticoDTO): Observable<DiagnosticoDTO>{
    return this.http.post<DiagnosticoDTO>(this.api,diagnosticoDTO)
  }

  delete(id:number): Observable<number>{
    return this.http.delete<number>(`${this.api}/${id}`)
  }


}

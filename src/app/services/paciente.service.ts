import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PacienteDTO } from '../modelos/paciente-dto.model';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  private api = `pacientes`

  private pacienteIdSource = new BehaviorSubject<number | null>(null)
  pacieteId$ = this.pacienteIdSource.asObservable()

  constructor(private http: HttpClient) { }

  setPacienteId(id : number | null): void{
    this.pacienteIdSource.next(id)
  }

  getAll(): Observable<PacienteDTO[]>{
    return this.http.get<PacienteDTO[]>(this.api)
  }

  getById(id:number): Observable<PacienteDTO>{
    return this.http.get<PacienteDTO>(`${this.api}/${id}`)
  }

  getByUsername(username:string): Observable<PacienteDTO>{
    return this.http.get<PacienteDTO>(`${this.api}/by-username/${username}`);
  }

  update(pacienteDTO:PacienteDTO): Observable<PacienteDTO>{
    return this.http.put<PacienteDTO>(this.api,pacienteDTO)
  }

  create(pacienteDTO:PacienteDTO): Observable<PacienteDTO>{
    return this.http.post<PacienteDTO>(this.api,pacienteDTO)
  }

  delete(id:number): Observable<number>{
    return this.http.delete<number>(`${this.api}/${id}`)
  }


}

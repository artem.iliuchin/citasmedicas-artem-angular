import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CitaDTO } from '../modelos/cita-dto.model';

@Injectable({
  providedIn: 'root',
})
export class CitaService {
  private api = `citas`;

  private citaIdSource = new BehaviorSubject<number | null>(null)
  citaId$ = this.citaIdSource.asObservable()

  constructor(private http: HttpClient) {}

  setCitaId(id: number | null): void {
    this.citaIdSource.next(id)
  }

  getAll(): Observable<CitaDTO[]> {
    return this.http.get<CitaDTO[]>(this.api)
  }

  getById(id: number): Observable<CitaDTO> {
    return this.http.get<CitaDTO>(`${this.api}/${id}`)
  }

  update(citaDTO: CitaDTO): Observable<CitaDTO> {
    return this.http.put<CitaDTO>(this.api, citaDTO)
  }

  create(citaDTO: CitaDTO): Observable<CitaDTO> {
    return this.http.post<CitaDTO>(this.api, citaDTO)
  }

  delete(id: number): Observable<number> {
    return this.http.delete<number>(`${this.api}/${id}`)
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MedicoDTO } from '../modelos/medico-dto-model';




@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  private api = `medicos`

  private medicoIdSource = new BehaviorSubject<number | null>(null)
  medicoId$ = this.medicoIdSource.asObservable()

  constructor(private http: HttpClient) { }

  setMedicoId(id: number | null): void{
    this.medicoIdSource.next(id)
  }

  getAll(): Observable<MedicoDTO[]>{
    return this.http.get<MedicoDTO[]>(this.api)
  }

  getById(id:number): Observable<MedicoDTO>{
    return this.http.get<MedicoDTO>(`${this.api}/${id}`)
  }

  getByUsername(username:string): Observable<MedicoDTO>{
    return this.http.get<MedicoDTO>(`${this.api}/by-username/${username}`)
  }

  update(medicoDTO:MedicoDTO): Observable<MedicoDTO>{
    return this.http.put<MedicoDTO>(this.api,medicoDTO)
  }

  create(medicoDTO:MedicoDTO): Observable<MedicoDTO>{
    return this.http.post<MedicoDTO>(this.api,medicoDTO)
  }

  delete(id:number): Observable<number>{
    return this.http.delete<number>(`${this.api}/${id}`)
  }

}

export const environment = {
  production: false,
  apiDomain: 'localhost',
  apiBaseUrl: 'http://localhost:8080',
};

export const environment = {
  production: true,
  apiDomain: 'localhost',
  apiBaseUrl: 'http://localhost:8080',
};
